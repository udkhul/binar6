package id.chalathadoa.chapter6.day1thread

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import id.chalathadoa.chapter6.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class Fragmentday1 : Fragment() {

    private lateinit var tvText: TextView

    /**
     * onCreateView, onViewCreated adalah UI thread/ Main Thread. Sedangkan mulai
     * dari Thread {} adalah thread baru
     */

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragmentday1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvText = view.findViewById(R.id.tv_text)
        onThreadClicked(view)
        onHandlerClicked(view)

        /**
         * COROUTINE
         */
        CoroutineScope(Dispatchers.IO).launch {

        }
        GlobalScope.launch {

        }

        //launch digunakan ketika kita tidak ingin balikan data
        //jika ingin balikan data pake suspend atau async

        /**
         * contoh:
         *  fun get launc() {
         *
         *  }
         *
         *  --List<Student> adalah balikan datanya
         *  fun getAssync() : List<Student> {
         *      return listOf()
         *  }
         */

        /**
         * Semua COroutine harus berjalan di Dispatchers walaupun ketika mereka berjalan di main thread.
         *
         * Ada 3 jenis dispatcher: Dispatcers.Main -> Main thread ; Dispatcers.IO -> Background Thread ; Dispatcers.Default -> akan menjadi default jika tidak ditulis dispatchernya yg apa
         *
         * untuk DIspatchers.IO diperuntukan untuk hal yg berat spt Mengambil data, database, membaca atau menulis file, dan networking
         * untuk Dispatcers.Default punya tugas: Sorting list; Parsing JSON; DiffUtils
         */
    }

    private fun onThreadClicked(view: View) {
        val btnThread = view.findViewById<Button>(R.id.btn_thread)
        btnThread.setOnClickListener {
            //membuat thread baru di luar main thread
            Thread{
                //tdk boleh memanggil ui toolkit di thread yg berbeda dari main thread

                /**
                 * tidak boleh langsung memanggil tools dari main thread langsung seperti dibawah
                 * tvText.text = "INI THREAD"
                 */

                //run on ui thread hanya milik activity
                requireActivity().runOnUiThread {

                }

                //ini yang memakai .post
                tvText.post{

                }

                //ini yg memakai .postDelayed jadi akan ada delay sebelum thread muncul
                tvText.postDelayed({
                    tvText.text = "run on UI Thread"
                }, 1000)

                /**
                 * Selain dari 3 cara diatas, kita bisa menggunakan handler untuk
                 * menghubungkan main thread ke thread ygkita buat, sehingga
                 * kita dapat mengakses tools dari main thread
                 */

            }.start()
        }
    }

    //bikin objek terlebih dahulu, karena handler adalah interface
    val handler = object : Handler (Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {

            val message = msg.obj as String

            tvText.text = message
        }
    }

    private fun onHandlerClicked(view: View){
        /**
         * Handler menggantikan runOnUiThread(), .post, dan .postDelayed
         */

        val btnHandler = view.findViewById<Button>(R.id.btn_handler)
        btnHandler.setOnClickListener {
            Thread{
                val msg  = Message.obtain()
                msg.obj = "run on Handler"

                //ada 4 cara untuk mengirim message pada handler.
                // sendEmptyMessage(); sendMessage(); sendMessageAtTime(); dan sendMessageDelayed()
                handler.sendMessage(msg)
                handler.sendMessageDelayed(msg, 1000)
                handler.sendMessageAtTime(msg, 1000)
            }.start()

            //dengan Handler, kita bisa memiliki banyak thread,
            //jika menggunakan .post dkk, maka kita perlu membikin manual threadnya satu"
            Thread{

            }.start()
        }
    }

    /**
     * AsyncTask adalah penolong doiantara Thread dan Handler. Idealnya digunakan untuk operasi pendek.
     * Kalau mau operasi yg panjang, pakai Executor, ThreadPoolExecutor, dan Futuretask
     *
     * Poin penting: Hanya untuk proses Asynccronus dan bisa komunikasi dgn Main Thread
     *               Class Java yg dibuat harus inherit ke AsyncTask (extend AsyncTask)
     *
     * AsyncTask dibagi mjd beberapa method utama yg akan berjalan ketika asynctask bekerja
     *    = onPreExecute()
     *    = doInBackground
     *    = onProgressUpdate
     *    = onPostExecute
     *
     * Contoh umum dari penggunaan asynctask ini adlaah download, jadi user bisa melihat berapa
     * progress download yg sudah berjalan. Hanya untuk proses yg ringan.
     */
}